// JavaScript Array

// Array Basic Structure
/* Syntax:
	let/const arrayName = [elementA, elementB, elementC....];
*/

let grades = [98.5, 94.3, 89.2, 90.1];

let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Possible use of mixed elements in an array but is not recommended

let mixedArr = [12, 'Asus', null, undefined, true];

let myTask = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
]

console.log(grades)
console.log(computerBrands[2], computerBrands[4]);
console.log(computerBrands[7]);

// Reassign Array Values
console.log('Array before reassignment');
console.log(myTask);
myTask[0] = 'hello world'
console.log('Array after reassignment');
console.log(myTask);

// Array Methods

	// Mutator Methods

		// - are functions that'mutate' or change an array after they're created

		let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon fruit'];

		// push()
			//  - adds an element in the end of an array and returns the array's length
			/* Syntax:
				arrayName.push()
			*/

			console.log('Current array:');
			console.log(fruits);
			let fruitsLength = fruits.push('Mango');
			console.log(fruitsLength);
			console.log('Mutated array from push method:');
			console.log(fruits);

		// pop()
			// Removes the last element in an array and returns the removed element
			/* Syntax:
				arrayName.pop()
			*/
			let removeFruit = fruits.pop();
			console.log(removeFruit);
			console.log('Mutated array from pop method:');
			console.log(fruits);

		// unshift()
			/*
				- adds one or more elements at the beginning of an array

				Syntax:
					arrayName.unshift('elementA');
					arrayName.unshift('elementA', 'elementB')
			*/

			fruits.unshift('Lime', 'Banana');
			console.log('Mutated array from unshift method:');
			console.log(fruits);

		// shift()
			/*
				- removes an element from the beginning of an array and returns the removed element

				Syntax:
					arrayName.shift();
			*/

			
			let anotherFruit = fruits.shift();
			console.log(anotherFruit);
			console.log('Mutated Array from shift method');
			console.log(fruits);

		// splice()
			/*
				- simultaneously removes elements from a specified index number and adds elements
				Syntax:
					arrayName.splice(startingIndex, deleteCount, elementToBeAdded)
			*/

			fruits.splice(1, 2, 'Lime', 'Cherry');
			console.log('Mutated array from splice method');
			console.log(fruits);

		// sort()
			/*
				- rearranges the array elements in alphanumeric order
			*/

			fruits.sort();
			console.log('Mutated array from sort method');
			console.log(fruits);

		// reverse()
			/*
				- reverses the order of array elements
			*/
			fruits.reverse();
			console.log('Mutated array from reverse method');
			console.log(fruits);

	// Non - mutator methods
	//  these are functions that do not modify or change an array after they're created

		let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

		// indexOf()
			/*
				- returns an index number of the first matching element found in an array
				- if no match was found, the result will be -1
				-Syntax:
					arrayName.indexOf(searchValue);
			*/
			let firstIndex = countries.indexOf('PH');
			console.log('Result of indexOf method: ' + firstIndex);

			let invalidCountry = countries.indexOf('BR');
			console.log('Result of indexOf method: ' + invalidCountry);

		// lastIndexOf()
			/*
				- returns the index number of the last matching element found in an array
				Syntaax:
					arrayName.lastIndexOf(searchValue);
					arrayName.lastIndexOf(searchValue, fromIndex)
			*/
			// getting the index number starting from the last element

			let lastIndex = countries.lastIndexOf('PH');
			console.log('Result of lastIndexOf method: ' + lastIndex);

			// getting the index number starting from a specified index backwards

			let lastIndexStart = countries.lastIndexOf('PH', 6);
			console.log('Result if lastIndexOf method: ' + lastIndexStart);

		// slice()
			/*
				- portions/slices elements from array and returns a new array
				Syntax:
					arrayName.slice(startingIndex);
					arrayName.slice(startingIndex, endingIndex)
			*/

			// slicing of elements from a specified index to the  last element
			console.log('Countries before slice: ' + countries)

			let slicedArrayA = countries.slice(2)
			console.log('Result of slice method')
			console.log(slicedArrayA)

			// slicing off elements from a specified index to another index

			let slicedArrayB = countries.slice(2, 4);
			console.log('Result from slice method:');
			console.log(slicedArrayB)

		// toString()
			/*
				- returns an array as a string separated by commas
				Syntax:
					arrayName.toString();
			*/

			let stringArray = countries.toString();
			console.log('Result from toString method:');
			console.log(stringArray);

		// concat()
			/*
				- combines two arrays and return the combined result
				Syntax:
					arrayA.concat(arrayB);
					arrayA.concat(elementA);
			*/

			let tasksArrayA = ['drink html', 'eat javascript'];
			let tasksArrayB = ['inhale css', 'breathe sass'];
			let tasksArrayC = ['get git', 'be node'];

			let tasks = tasksArrayA.concat(tasksArrayB);
			console.log('Result from concat method:');
			console.log(tasks);
			
			// Combining multiple arrays
				console.log('Result from concat method: ');
				let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
				console.log(allTasks);

			// Combining array wiht elements
			let combinedTasks = tasksArrayA.concat('smell express', 'throw react')
			console.log('Result from concat method:')
			console.log(combinedTasks)

		// join()
			/*
				- returns an array as a string separated by specified separator string
				Syntax:
					arrayName.join('separatorString');
			*/

			let users = ['John', 'Jane', 'Joe', 'Robert'];
			console.log(users.join());
			console.log(users.join(' +++ '));